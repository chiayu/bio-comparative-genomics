package GetCompara;
use strict;
use Bio::EnsEMBL::Registry;
use Bio::EnsEMBL::Compara::DBSQL::GenomicAlignBlockAdaptor;
use Bio::EnsEMBL::Utils::Exception qw(throw);
use Bio::EnsEMBL::Compara::AlignSlice;
use Bio::SimpleAlign;
use Bio::AlignIO;
use Bio::LocatableSeq;
use Bio::SeqIO;
use Bio::Seq;
use Bio::SearchIO;
use Bio::EnsEMBL::Compara::AlignSlice;
use Statistics::Descriptive;

#new object ->new('_alignment_type' => 'LASTZ_NET' )

sub new {
	my ( $class, %args ) = @_;
	my $self = bless {}, $class;

	$self->{'_coord_system'}   = $args{'-coord_system'};
	$self->{'_alignment_type'} = $args{'-alignment_type'};
	$self->{'_conf_file'}      = $args{'-conf_file'};
	$self->{'_criteria'}       = $args{'_criteria'} || 85;

	Bio::EnsEMBL::Registry->load_all( $self->{'_conf_file'} );

	return $self;
}

# Description: Get number of alignments of  gene A (in specieA) to other species
# Return Type: number
# Arg 1 : String, specieA
# Arg 2 : String, stable id of gene A in specieA
# Arg 3 : Hasf ref, which represent all the other strains

sub get_num_of_align_from_species {

	my $self = shift @_;
	my ( $specieA, $stable_id, $species_array_ref ) = @_;

	my $flag           = 0;
	my @set_of_species = @{$species_array_ref};

	foreach my $species (@set_of_species) {
		if (
			$species ne $specieA
			&& get_simple_align_to_another(
				$self, $species, $specieA, $stable_id
			)->{"num_of_gabs"} > 0
		  )
		{
			$flag++;
		}
	}

	return $flag;
}

# Description: Get alignment of slice (in spciecA) align to specieB.
# Return Type: hash reference whiche contain {"ref_sa', 'score', 'num_of_gabs"}
# Arg 1 : String, spciecB
# Arg 2 : String, spciecA
# Arg 3 : Bio::EnsEMBL::Slice , slice of spciecsA or ENSEMBL GENE STABLE ID
# Arg [4] : String, testing mode, default is undef

sub get_simple_align_to_another {

	my $self = shift @_;
	my ( $specieB, $specieA, $specieA_slice, $stat_mode ) = @_;

	my @arr = ( $specieB, $specieA );

	# Getting all the Bio::EnsEMBL::Compara::GenomeDB objects
	my $genome_dbs;
	my $genome_db_adaptor =
	  Bio::EnsEMBL::Registry->get_adaptor( 'compara', 'compara', 'GenomeDB' );

	foreach my $this_species (@arr) {

		my $this_meta_container_adaptor =
		  Bio::EnsEMBL::Registry->get_adaptor( $this_species, 'core',
			'MetaContainer' );

		throw(
"Registry configuration file has no data for connecting to <$this_species&"
		) if ( !$this_meta_container_adaptor );
		my $this_production_name =
		  $this_meta_container_adaptor->get_production_name;
		my $genome_db =
		  $genome_db_adaptor->fetch_by_name_assembly($this_production_name);
		push( @$genome_dbs, $genome_db );
	}

	# Getting Bio::EnsEMBL::Compara::MethodLinkSpeciesSet object
	my $method_link_species_set_adaptor =
	  Bio::EnsEMBL::Registry->get_adaptor( 'compara', 'compara',
		'MethodLinkSpeciesSet' );

	my $method_link_species_set =
	  $method_link_species_set_adaptor->fetch_by_method_link_type_GenomeDBs(
		$self->{'_alignment_type'}, $genome_dbs );

	throw(  "The database do not contain any "
		  . $self->{'_alignment_type'}
		  . " data for " )
	  if ( !$method_link_species_set );

	# Fetching the query Slice:
	my $query_slice;
	if ( ref($specieA_slice) eq "Bio::EnsEMBL::Slice" ) {
		$query_slice = $specieA_slice;
		throw("No Slice $specieA_slice") if ( !$query_slice );
	}
	else {
		my $slice_adaptor =
		  Bio::EnsEMBL::Registry->get_adaptor( $specieA, 'core', 'Slice' );
		my $gene_adaptor =
		  Bio::EnsEMBL::Registry->get_adaptor( $specieA, "core", "gene" );

		throw("No Slice to <$specieA_slice>") if ( !$slice_adaptor );

		my $gene = $gene_adaptor->fetch_by_stable_id($specieA_slice);
		$query_slice = $gene->feature_Slice;
	}

	# Fetch the align_slice
	my $align_slice_adaptor =
	  Bio::EnsEMBL::Registry->get_adaptor( "compara", "compara", "AlignSlice" );

	# Setting return value
	my ( $ref_sa, $percentage_identity, $num_of_slices ),

	  my $align_slice =
	  $align_slice_adaptor->fetch_by_Slice_MethodLinkSpeciesSet( $query_slice,
		$method_link_species_set, 'expanded', 'restrict', undef );

    #there would be lots of warnings...
	$percentage_identity =
	$align_slice->get_SimpleAlign()->overall_percentage_identity;

    
    #if no alignment, it will show 100
	if ( $percentage_identity eq 100 ) { $percentage_identity = 0; }

	$ref_sa = $align_slice->get_SimpleAlign();

	my $all_slices = $align_slice->get_all_Slices($specieB);

	foreach my $sl (@$all_slices) {
		my $under_slices = $sl->get_all_underlying_Slices;
		$num_of_slices = scalar(@$under_slices);
	}

	if ($stat_mode) {
		print "Number of slice: " . scalar(@$all_slices) . "\n";

		#my $out = Bio::AlignIO->newFh(-fh=>\*STDOUT, -format=> "clustalw");
		#print $out $align_slice->get_SimpleAlign();

		foreach my $sl (@$all_slices) {
			my $under_slices = $sl->get_all_underlying_Slices;
			print "Number of underlying slice: "
			  . scalar(@$under_slices) . "\n";
			foreach my $under_sl (@$under_slices) {
				print $under_sl->display_id . "\n";
			}

		}

	}

	return {
		'ref_sa'      => $ref_sa,
		'score'       => $percentage_identity,
		'num_of_gabs' => $num_of_slices,
		'is_artifact' => &_is_artifact( $self, $percentage_identity ),
	};

}

# Description: Get difference of a slice (in spciecA) align to other species which written in group.txt.
# Return Type: hash reference whiche contain {"ref_sa', 'score', 'num_of_gabs"}
# Arg 1 : Bio::EnsEMBL::Slice
# Arg [2] : String , reprenseting group.txt, if undef, it will count all groups.
# Arg [3] : String , reprenseting exception_group.txt, default is null.
# Arg [4] : String, testing mode, default is undef
sub get_diff_from_slice_in_alignment_level {

	my $self = shift @_;
	my ( $slice, $group_file, $expection_file ) = @_;

	my $ref_specie = $slice->adaptor->db()->species();


	#Load the group.txt
	my @all_species;
	if ($group_file) {
		open( FILE, $group_file ) or die "$! cannot open $group_file";
		@all_species = map { chomp; $_; } <FILE>;
		close FILE;
	}
	else {
		@all_species = @{ Bio::EnsEMBL::Registry->get_all_species() };
	}

	#Load exception.txt
	my %hash_expect_species;
	if ($expection_file) {
		open( FILE, $expection_file ) or die "$! cannot open $expection_file";
		my @arr_expect_species = map { chomp; $_; } <FILE>;
		%hash_expect_species = map { $_ => 0 } @arr_expect_species;
		close FILE;
		@all_species = map {
			if   ( $hash_expect_species{$_} ) { }
			else                              { $_; }
		} @all_species;
	}

	my @ref_sa_ary;
	my $total_aln_num = 0;

	#count statistic difference
	my $stat         = Statistics::Descriptive::Full->new();
	my @arr_for_stat = ();

	foreach my $other_specie (@all_species) {
		if (   $other_specie ne $ref_specie
			&& $other_specie ne "helicobacter_pylori_sf7" )
		{

			my $ref_sa =
			  &get_simple_align_to_another( $self, $other_specie, $ref_specie,
				$slice );

			$total_aln_num = $total_aln_num + $ref_sa->{"num_of_gabs"};

			$stat->add_data( $ref_sa->{'score'} );
			push( @arr_for_stat, $ref_sa->{'score'} );

			if ($ref_sa) {
				push( @ref_sa_ary, $ref_sa->{'ref_sa'} );
			}

		}
	}

	return {
		'num_of_gbas' => $total_aln_num,
		'ref_sa'      => \@ref_sa_ary,
		'arr'         => \@arr_for_stat,
		'mean'        => $stat->mean(),
		'variance'    => $stat->variance(),
		'sum'         => $stat->sum(),
		'std'         => $stat->standard_deviation(),
	};

}

sub _is_artifact {

	my $self = shift @_;
	my ($percentage_identity) = @_;

	if ( $percentage_identity > $self->{'_criteria'} ) {
		return 1;
	}
}

1;
