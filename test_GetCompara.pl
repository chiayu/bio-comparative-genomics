#!/usr/bin/perl
##Author: Chia-Yu Hsu
##Date: 2015-03-30
##Aim:
##Environment:
##Usage:
##
use strict;
use GetCompara;
my $conf_file = "/home/u00cyh00/project/ensembl_services/comparative_analysis/conf/registry.conf.23";
my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);

my %STRAINS = map { $_ => 0 } @{$REGISTRY->get_all_species()} ;

&test_get_num_of_align_to_another();

sub test_get_num_of_align_to_another{

    #my $test_stable_id = "ENSHPG0000002413";
    my $test_stable_id = "ENSHPG0000000083";
    #my $test_stable_id = "ENSHPG0000035051";
    my $compara = GetCompara->new('-alignment_type' => 'LASTZ_NET', '-conf_file' => $conf_file );
    #my $num =  $compara->get_num_of_align_to_another("helicobacter_pylori_hm2", "helicobacter_pylori_hm1",$test_stable_id);
    #my $score =  $compara->get_score_of_align_to_another("helicobacter_pylori_hm2", "helicobacter_pylori_hm1",$test_stable_id);
    my $score =  $compara->get_sit_of_align_to_another("helicobacter_pylori_gu1", "helicobacter_pylori_sf7",$test_stable_id ,"test");
    #my $score =  $compara->get_score_of_align_to_another("helicobacter_pylori_du3", "helicobacter_pylori_gu5",$test_stable_id);
    print "\n";
    print $score->{"coverage"}."\nend of script\n";
    print $score->{"is_exists_gene"}."\nend of script\n";
}
