#!/usr/bin/env perl
##Author: Chia-Yu Hsu
##Date: 2015-04-13

=pod

=head1 NAME

       get_align_identity_overall.pl - Using GetCompara.pm,  

=head1 SYNOPSIS
       
        perl get_align_identity_overall.pl -conf [file ...] -ref [reference name] -window_size 2000

       Options:
        -help            brief help message
        -conf            file, registry.conf
        -ref             string, helicobacter_pylori_du1
        -window_size     int, 2000
        
=head1 DESCRIPTION

        output as following:
        <seq region name>   <start> <end>   <group1 file name>   <sum>   <mean>  <std>  <group1 file name>   <sum>   <mean>  <std>...
        contig_u1 201   301   ulcer.txt 17.2 1.7 37.93 77  hm.txt    1347.0094   122.4554    1011.283353 74
        contig_u1 191   201   ulcer.txt 14.4 1.4 23.71 51  hm.txt    1329.786505 120.8896823 889.3734147 53
        output for Wilcoxon Test:
        Probability:   0.438580, normal approx w. mean: 121.000000, std deviation:  14.200939, z:  -0.774597
        
=cut

use strict;
use warnings;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Registry;
use Bio::AlignIO;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);
use Statistics::Descriptive;
use GetCompara;

my ( $help, $conf_file, $ref_specie, $window_size, $group_file );

GetOptions(
    '-help|h'        => \$help,
    '-conf=s'        => \$conf_file,
    '-ref=s'         => \$ref_specie,
    '-window_size=i' => \$window_size,
    '-group=s'       => \$group_file,
);

pod2usage( -verbose => 1 ) if $help;

my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);

&main;

sub main {

    my $alignment_type = "LASTZ_NET";

    my $slice_adaptor =
      Bio::EnsEMBL::Registry->get_adaptor( $ref_specie, 'core', 'Slice' );

    my $slices = get_slices_from_sliding_window( $window_size, $slice_adaptor );
    my $compara = GetCompara->new(
        '-alignment_type' => 'LASTZ_NET',
        '-conf_file'      => $conf_file
    );

    #print and compare between groups slice by slice
    foreach my $slice ( @{$slices} ) {

        &print_slice_info($slice);

        my $dif =
          $compara->get_diff_from_slice_in_alignment_level( $slice,
            $group_file );

        my @ref_sa_ary = @{ $dif->{'ref_sa'} };
  
        my $a_gene_aln = &merge_align( \@ref_sa_ary, $species );

        &_output_aln_result( $a_gene_aln, $output_format );        
        
        &print_diff_for_group( $group_file, $dif );

        &print_slice_info();
    }

}

sub get_slices_from_sliding_window {

    my ( $window_size, $slice_adaptor ) = @_;

    my $all_slices = $slice_adaptor->fetch_all('seqlevel');

    my @total_slices_by_sliding = ();

    foreach my $one_slice (@$all_slices) {

        my $limit_length = ( $one_slice->length ) - 1 - $window_size;

        for ( my $i = 1 ; $i <= $limit_length ; $i = $i + $window_size ) {

            my $slice =
              $slice_adaptor->fetch_by_region( "contig",
                $one_slice->seq_region_name(),
                $i, $i + $window_size );
            $slice->seq;    # if I don't call it , it leads to MYSQL::st Error
            push( @total_slices_by_sliding, $slice );
        }

    }

    return \@total_slices_by_sliding;

}

sub print_slice_info {
    my ($slice) = @_;
    if ($slice) {
        print $slice->seq_region_name . "\t"
          . $slice->start . "\t"
          . $slice->end . "\t";
    }
    else {
        print "\n";
    }
}

sub print_diff_for_group {

    my ( $group_file, $diff ) = @_;

    if ($group_file) {
        print "$group_file\t",;
    }
    else {
        print "ALL\t",
    }
    print $diff->{'mean'} . "\t", $diff->{'std'} . "\t" ;

}


sub _output_aln_result {
  my $a_gene_aln    = shift;
  my $output_format = shift;
  my $alignIO       = Bio::AlignIO->newFh(

    #-interleaved => 0,
    -fh       => \*STDOUT,
    -format   => $output_format,
    -idlength => 40
  );
  print $alignIO $a_gene_aln;

  #foreach my $ref_sa (@$ref_sa_ary) {
  #print $alignIO $a_gene_aln;
  #}
}


sub merge_align {
  my $ref_sa_ary = shift;
  my $species    = shift;

  my $a_gene_aln;
  foreach my $this_aln (@$ref_sa_ary) {

    my $a_simpleAln = $this_aln;
    if ( !defined $a_gene_aln ) {
      $a_gene_aln = $a_simpleAln;
    }
    else {
          
      ( $a_gene_aln, $a_simpleAln ) =
        __perfect_simple_alns( $a_gene_aln, $a_simpleAln, $species );
      my $i = 2;
      while ($i <=  $a_simpleAln->num_sequences) {
            
        $a_gene_aln->add_seq( $a_simpleAln->get_seq_by_pos($i) );   
        $i++;
      }
      
      
     

    }
  }
   
  return $a_gene_aln;
}


sub get_alignment_strain {
  my $species        = shift;
  my $AB_strains_ref = shift;
  my @set_of_species_ary;

  #my $set_of_species   = "a_baumannii_2008p87:a_baumannii_sdf";

  foreach my $strain (@$AB_strains_ref) {
    if ( $strain eq $species ) {
      next;
    }
    else {
      my $set_of_species = $species . ":" . $strain;
      push( @set_of_species_ary, $set_of_species );
    }
  }
  return \@set_of_species_ary;
}



sub check_aln_start_position {
  my ( $aln1, $aln2, $species ) = @_;
  my @compare_aln;

  foreach my $a ( keys %{ $aln1->{'_seq'} } ) {
    my @aln1_info = split( /\//, $a );
    if ( $aln1_info[0] eq $species ) {
      my @aln1_pos = split( '-', $aln1_info[2] );
      push( @compare_aln, $aln1_pos[0] );
    }
  }

  foreach my $a ( keys %{ $aln2->{'_seq'} } ) {
    my @aln2_info = split( /\//, $a );
    if ( $aln2_info[0] eq $species ) {
      my @aln2_pos = split( '-', $aln2_info[2] );
      push( @compare_aln, $aln2_pos[0] );
    }
  }

  if ( $compare_aln[0] ne $compare_aln[1] ) {
    my $aln_different = $compare_aln[0] - $compare_aln[1];
    return $aln_different;
  }
  else {
    return;
  }

}

sub __perfect_simple_alns {
  my $aln1    = shift;
  my $aln2    = shift;
  my $species = shift;

  my $ref_locseq1 = $aln1->get_seq_by_pos(1);
  my $ref_locseq2 = $aln2->get_seq_by_pos(1);

  my $ref_seq1 = $ref_locseq1->seq;
  my $ref_seq2 = $ref_locseq2->seq;

  my @ref_seq1 = split( //, $ref_seq1 );
  my @ref_seq2 = split( //, $ref_seq2 );

  my $aln_difference_pos = check_aln_start_position( $aln1, $aln2, $species );

  if ( $aln_difference_pos && $aln_difference_pos ne 0 ) {
    my $count = abs($aln_difference_pos);
    while ( $count > 0 ) {
      if ( $aln_difference_pos > 0 ) {
        unshift( @ref_seq2, '-' );
        $count--;
      }
      elsif ( $aln_difference_pos < 0 ) {
        unshift( @ref_seq1, '-' );
        $count--;
      }

    }
  }

  my $ref_seq1len     = scalar(@ref_seq1);
  my $ref_seq2len     = scalar(@ref_seq2);
  my $idx1            = 0;
  my $idx2            = 0;
  my %seq1_newgap_pos = ();
  my %seq2_newgap_pos = ();

  while ( $idx1 < $ref_seq1len || $idx2 < $ref_seq2len ) {

    #because original code will produce uninitalized value
    #so assinged the  value
    if ( !$ref_seq1[$idx1] ) {
      $ref_seq1[$idx1] = '';
      print $ref_seq1[$idx1];
    }
    elsif ( !$ref_seq2[$idx2] ) {
      $ref_seq2[$idx2] = '';
    }

    if ( ( $ref_seq1[$idx1] eq '-' ) xor( $ref_seq2[$idx2] eq '-' ) ) {
      if ( $ref_seq1[$idx1] eq '-' ) {
        $seq2_newgap_pos{$idx2}++;
        $idx1++;
      }
      else {
        $seq1_newgap_pos{$idx1}++;
        $idx2++;
      }
    }

    else {
      $idx1++;
      $idx2++;
    }
  }

  $aln1 = ___insert_gaps_into_simple_aln( $aln1, \%seq1_newgap_pos );
  $aln2 = ___insert_gaps_into_simple_aln( $aln2, \%seq2_newgap_pos );

  # exit;

  return $aln1, $aln2;
}

sub ___insert_gaps_into_simple_aln {
  my $aln      = shift;
  my $gap_list = shift;

  if ( scalar( keys %$gap_list ) == 0 ) {
    return $aln;
  }

  my @seqs        = $aln->each_seq;
  my $ref_seq_txt = $seqs[0]->seq;
  my $ref_seq_len = length($ref_seq_txt);

  my @new_seqs_txt = ();
  for ( my $i = 0 ; $i < scalar(@seqs) ; $i++ ) {
    $new_seqs_txt[$i] = '';
    my @current_seq_txt = split( //, $seqs[$i]->seq );

    for ( my $j = 0 ; $j < $ref_seq_len ; $j++ ) {

      #because original code will produce uninitalized value
      #so assinged the  value
      if ( !$new_seqs_txt[$i] ) {
        $new_seqs_txt[$i] = '';
      }
      elsif ( !$current_seq_txt[$j] ) {
        $current_seq_txt[$j] = '';
      }

      if ( exists $gap_list->{$j} ) {
        $new_seqs_txt[$i] .= '-' x $gap_list->{$j};
      }
      $new_seqs_txt[$i] .= $current_seq_txt[$j];
    }
    $seqs[$i]->seq( $new_seqs_txt[$i] );

    #   $seqs[$i]->alphabet('dna');
  }

  # print 'seq0:'.$seqs[0]->seq, "\n";
  return $aln;
}
