#!/usr/bin/perl
##Author: Chia-Yu Hsu
##Date: 2015/1/8
##Last update:  2015/8/24
##Environment:  Ensemble API version 75, BioPerl-1.6.923

=pod

=head1 NAME

generate_gene_table.pl - Using GetCompara.pm
       
=head1 SYNOPSIS
       
perl generate_gene_table.pl -conf [file ...]


=head1 OPTIONS

=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-conf>

Registry.conf file

=item B<-mode>

Check every 0 alignment has no alignments to others

=item B<-t   >

Transfer every number bigger than 1 to 1.

=back

=head1 DESCRIPTION
           
        This program generates gene table in STDOPT as following:

        gene num: 123
        <tab>   human   mus <Null>
        BRCA1   2       3   ENSP0001,ENSP0002....
        BRCA2   1       9   ENSP0005,ENSP0003....
        
        
        STDERR as following while using '-mode' 
        
        ENSHPG0000022344 of helicobacter_pylori_du3 v.s. helicobacter_pylori_sf7  may have prediction error
        coverge: 91.904047976012
        RANK:1

=head1 CHECK ALIGNMENT PRINCIPLE
        
        Here is a tree of protein A, and specise num is 4.
            
        --------------hm1
        /
        /
        ----/---------hm2
            /
            ----------hm3


        It's observed that hm4 has no protein. 
        Then we will check hm1 v.s. hm4, 
                           hm2 v.s. hm4, 
                           hm3 v.s. hm4.
                           
        if there is any one strain which has alignments to hm4 and identity bigger than 85%,
        it is considered that there is a prediction error.
        


=head1 EXAMPLE

perl generate_gene_table.pl -conf conf/registry.conf.23 -mode -t 1>gene_table.txt 2>gene_table.err

=cut

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::GeneTree;
use Bio::EnsEMBL::Compara::MemberSet;
use Bio::EnsEMBL::Registry;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);
use GetCompara;

my ( $CHECK, $TRANSFER, $help, $conf_file );

GetOptions(
	'help|h' => \$help,
	'conf=s' => \$conf_file,
	'mode'   => \$CHECK,
	't'      => \$TRANSFER,
);

pod2usage( -verbose => 2 ) if $help;

#check parameter already loaded
check_parameters();


#build ensembp registry for connect to database
my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);
my %STRAINS = map { $_ => 0 } @{ $REGISTRY->get_all_species() };

#the main function
&main;

#define main function
sub main {

    #get adapter of gene tree
	my $geneTree_adaptor =
	  $REGISTRY->get_adaptor( 'compara', 'compara', 'GeneTree' );

    #get all tress
	my @trees = @{
		$geneTree_adaptor->fetch_all(
			'-CLUSTERSET_ID' => "default",
			'-TREE_TYPE'     => 'tree',
		)
	  };

	#print how many trees I have();
	print "Number of tree : " . scalar(@trees) . "\n";
	print "x\t";
	print join( "\t", ( sort keys %STRAINS ) );
	print "\n";

	#print every tree
	foreach my $tree (@trees) {

		my $leaves = $tree->get_all_leaves();
		&print_tree( \%STRAINS, $leaves );

	}

}

#defined the print_tree function
sub print_tree {

    #get parameters
	my $all_strains_ref = shift;
	my $leaves_ref      = shift;

    #transfer reference to the hash object
	my %all_strains_counter        = %{$all_strains_ref};
	my %all_strains_gene_stable_id = %{$all_strains_ref};
	my $stable_id;
	my $description;
	my $gene_adaptor;
	my $gene;

    #check the species in the tree
	foreach my $member (@$leaves_ref) {

        #get specie name
		my $strain = $member->genome_db->name;
		$all_strains_counter{$strain} = $all_strains_counter{$strain} + 1;

        #get adapter of this specie
		$gene_adaptor =
		  $REGISTRY->get_adaptor( $member->genome_db->name, "core", "gene" );

        #get stable id of this member
		$stable_id = $member->stable_id;

        #get the gene by the stable id 
		$gene = $gene_adaptor->fetch_by_translation_stable_id($stable_id);
		$description = $gene->description;
        
        #give value to the hash
		$all_strains_gene_stable_id{$strain} = $gene->stable_id;
	}

	#check the alignment level
	if ($CHECK) {
	    #for every exist species
		foreach my $strain ( keys %all_strains_counter ) {
		    #if it's not exist
			if ( $all_strains_counter{$strain} == 0 ) {
			
			    #new the object of GetCompara.pm
				my $compara = GetCompara->new(
					'-alignment_type' => 'LASTZ_NET',
					'-conf_file'      => $conf_file
				);
				#new a variance respesent whether alignment to anothor specie
				my $is_align = 0;
				foreach my $other_strain ( keys %all_strains_gene_stable_id ) {
					if (    $strain ne $other_strain
						and $all_strains_gene_stable_id{$other_strain} )
					{

						my $compara_sit =
						  $compara->get_simple_align_to_another( $strain,
							$other_strain,
							$all_strains_gene_stable_id{$other_strain} );

						my $is_artifact;
						$is_artifact = 1 if ( $compara_sit->{"score"} > 85 );

						$is_align = $is_align + $is_artifact;

						if ( $is_artifact == 1 ) {
							print
							  STDERR $all_strains_gene_stable_id{$other_strain}
							  . " of $other_strain v.s. $strain  may have prediction error \n";
							print STDERR "Coverge: "
							  . $compara_sit->{"score"} . "\n";
							print STDERR "RANK:" . $is_align . "\n";

						}
					}
				}

				$all_strains_counter{$strain} = $is_align;

			}

		}
	}

	#transfer table to zero and 1
	if ($TRANSFER) {
		foreach my $strain ( keys %all_strains_counter ) {
			if ( $all_strains_counter{$strain} >= 1 ) {
				$all_strains_counter{$strain} = 1;
			}
		}
	}

	#print tree to one line

	print $description. "\t";

	foreach my $strain ( sort keys %all_strains_counter ) {
		print $all_strains_counter{$strain} . "\t";
	}

	print "\t";

	foreach my $strain ( sort keys %all_strains_counter ) {
		print $all_strains_gene_stable_id{$strain} . ", ";
	}
	print "\n";

}

sub check_parameters {

	print "Enter your registry.conf path!\n\n" if ( !$conf_file );

}
