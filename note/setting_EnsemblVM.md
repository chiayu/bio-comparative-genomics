##	Ensembl VM
----
1. 下載Ensembl官方VM
	* ftp://ftp.ensemblgenomes.org/pub/release-22/virtual_machines/EG_22_browser.ova
2. 安裝Virtual Box
3. 匯入EG_22_browser.ova



##	進入VM
----
預設ssh port:8022


*	VM

		account:eguser
		password:egpass

*	VM mysql
	
		account: ensrw
		password: ensrw

##	MySQL database 設定

1. 匯入ensembl_compara database
	* 已merge完,
	* ex :ensembl\_compara\_hp\_75
    * insert into method_link (method_link_id,type, class) values (6, "TRANSLATED_BLAT", "GenomicAlignBlock.pairwise_alignment");
    [method link](http://www.germonline.org/info/using/api/compara/compara_schema.html#method_link)
	* alter table member\_production\_counts add homoeologues int unsigned;

2. 匯入物種database
	* ex:helicobacter\_pylori\_hm1\_75

3. 匯入[ensembl\_compara\_pan\_homology\_22\_75](ftp://ftp.ensemblgenomes.org/pub/release-22/pan_ensembl/mysql/ensembl_compara_pan_homology_22_75/)
	* 因為檔案全部會很大，可匯入schema就好
	*	ftp://ftp.ensemblgenomes.org/pub/release-22/pan_ensembl/mysql/ensembl_compara_pan_homology_22_75/   **ensembl_compara_pan_homology\_22\_75.sql.gz**

4.	匯入 [ensembl\_production\_75](ftp://ftp.ensembl.org/pub/release-75/mysql/ensembl_production_75/)（同理，可匯入schema就好）

5. 匯入 [ensembl\_archive_75](ftp://ftp.ensembl.org/pub/release-75/mysql/ensembl_archive_75/)
    *   須將完整資料庫匯入
    

6.	Bacteria site 需匯入 [ensemblgenomes\_ontology\_22\_75	](ftp://ftp.ensemblgenomes.org/pub/release-22/pan_ensembl/mysql/ensemblgenomes_ontology_22_75/)

7. 匯入ensembl\_compara\_hp\_75中 member\_production\_counts這個table
	*	利用populate\_member\_production\_counts\_table

			mysql -uensrw -pensrw ensembl_compara_hp_75<ensembl-compara/scripts/production/populate_member_production_counts_table.sql
	* 匯入後會產生訊息(in terminal)
	
			Populating stable_id column in the member_production_counts table
			Creating temporary temp_member_family_counts table
			Populating the temporary temp_member_family_counts table
			Populating the families column in member_production_counts
			Creating temporary temp_member_tree_counts table
			Populating the temporary temp_member_tree_counts table
			Populating the gene_trees column in member_production_counts
			Populating the gene_gain_loss_trees column in member_production_counts
			Creating temporary temp_member_orthologues_counts table
			Populating the temporary temp_member_orthologues_counts table
			Populating the orthologues column in member_production_counts
			Creating temporary temp_member_paralogues_counts table
			Populating the temporary temp_member_paralogues_counts table
			Populating the paralogues column in member_production_counts





##	Introduction Ensembl VM structure(unfinished)
----
####	/ensembl/ensembl-genomes/release-22
*	以此路徑為中心
*	
		[eguser@localhost release-22]$ ll
		total 225552
		drwxr-xr-x.  8 eguser eguser      4096 Apr 10  2014 eg-web-bacteria
		drwxr-xr-x. 10 eguser eguser      4096 Apr  9  2014 eg-web-common
		drwxr-xr-x.  7 eguser eguser      4096 Apr 10  2014 eg-web-fungi
		drwxr-xr-x.  7 eguser eguser      4096 Apr 10  2014 eg-web-metazoa
		drwxr-xr-x.  7 eguser eguser      4096 Apr 10  2014 eg-web-plants
		drwxr-xr-x.  7 eguser eguser      4096 Apr 10  2014 eg-web-protists
		drwxr-xr-x.  7 eguser eguser      4096 Apr  9  2014 eg-web-search
		drwxr-xr-x.  8 eguser eguser      4096 Apr  9  2014 ensembl
		drwxr-xr-x.  7 eguser eguser      4096 Apr  9  2014 ensembl-compara
		drwxr-xr-x.  7 eguser eguser      4096 Apr  9  2014 ensembl-funcgen
		drwxr-xr-x.  4 eguser eguser      4096 Apr  9  2014 ensembl-orm
		drwxr-xr-x.  4 eguser eguser      4096 Apr  9  2014 ensembl-tools
		drwxr-xr-x.  9 eguser eguser      4096 Apr  9  2014 ensembl-variation
		drwxr-xr-x. 13 eguser eguser      4096 Apr  9  2014 ensembl-webcode
		drwxrwxr-x.  2 eguser eguser      4096 Oct 22 16:10 logs
		drwxr-xr-x. 18 eguser eguser      4096 Apr  9  2014 public-plugins
		drwxrwxr-x.  5 eguser eguser      4096 Apr 10  2014 tmp
		-rw-rw-r--.  1 eguser eguser 230893522 Apr 10  2014 web_assets.tar.gz


*	各種不同的Ensembl Genomics 站
			
		eg-web-bacteria 對應 [http://bacteria.ensembl.org/index.html](http://bacteria.ensembl.org/index.html "http://bacteria.ensembl.org/index.html")
		eg-web-common 指的是大家都要用到的一些設定
		eg-web-fungi
		eg-web-metazoa
		eg-web-plants
		eg-web-protists
		eg-web-search
*	套件與WebCore及其他

		ensembl
		ensembl-compara
		ensembl-funcgen
		ensembl-orm
		ensembl-tools
		ensembl-variation
		ensembl-webcode
		logs
		public-plugins
		tmp 

##	使用Ensembl 主站Webcore
----
*	/ensembl/ensembl-genomes/release-22/ensembl-webcode/conf/Plugins.pm
*	決定使用哪些Plugin

		$SiteDefs::ENSEMBL_PLUGINS = [
			'EnsEMBL::Mirror'     => $SiteDefs::ENSEMBL_SERVERROOT.'/public-plugins/mirror',
			'EnsEMBL::Genoverse'  => $SiteDefs::ENSEMBL_SERVERROOT.'/public-plugins/genoverse',
			'EnsEMBL::Ensembl'    => $SiteDefs::ENSEMBL_SERVERROOT.'/public-plugins/ensembl'
		];



###	設定ini file
*	/ensembl/ensembl-genomes/release-22/public-plugins/mirror/conf/ini-files
*	物種ini file
*	Example:Helicobacter_pylori_hm1.ini
*	檔案首字需大寫

		[general]
		ENSEMBL_GENOME_SIZE = 0.1
		COLLECTION_DEFAULT_FAVOURITES =
		SPECIES_RELEASE_VERSION = 1

		[databases]
		DATABASE_CORE = helicobacter_pylori_hm1_core_75_1
		;DATABASE_USERDATA = bacteria_1_collection_userdata

		[DATABASE_USERDATA]

		[ENSEMBL_STYLE]

		[ENSEMBL_EXTERNAL_URLS]

		[ENSEMBL_INTERNAL_DAS_SOURCES]

		[ENSEMBL_SPECIES_SITE]

		[SPECIES_DISPLAY_NAME]

		[ENSEMBL_EXTERNAL_DATABASES]
		; DO NOT REMOVE THIS HEADER - defined in DEFAULTS.ini

		[ENSEMBL_EXTERNAL_INDEXERS]
		; DO NOT REMOVE THIS HEADER - defined in DEFAULTS.ini





*	MULTI.ini
*	ensembl_compara_hp_75 =>自己合併後的compara database

		[databases]

		;DATABASE_CORE          = ensembl_ancestral_67
		DATABASE_COMPARA       = ensembl_compara_hp_75
		;DATABASE_GO            = ensembl_ontology_67

*	DEFAULTS.ini
*	設定在網頁上呈現的初始值
		
		[general]
		DATABASE_HOST       = localhost
		DATABASE_HOST_PORT  = 3306
		DATABASE_WRITE_USER = ensrw
		DATABASE_WRITE_PASS = ensrw
		DATABASE_DBUSER     = ensrw
		DATABASE_DBPASS     = ensrw
		
		DEFAULT_FAVOURITES = [ Helicobacter_pylori_hm1 Helicobacter_pylori_hm2 ]
		; ENSEMBL_WWW_PROXY            = http://my_webcache.my_domain.org:3128

		...(節錄前面有更改的部分)



###	設定Site.pm
*	/ensembl/ensembl-genomes/release-22/public-plugins/ensembl/conf/SiteDefs.pm
*	啟動時候決定了哪些物種要被匯入
		
		$SiteDefs::ENSEMBL_PRIMARY_SPECIES = 'Helicobacter_pylori_hm1';
		$SiteDefs::ENSEMBL_SECONDARY_SPECIES = 'Helicobacter_pylori_hm2';
		(一定要設置上面兩個)
		$SiteDefs::__species_aliases{ 'Helicobacter_pylori_hm1'       } = [qw(hm1)];
		$SiteDefs::__species_aliases{ 'Helicobacter_pylori_hm2'       } = [qw(hm2)];
		$SiteDefs::__species_aliases{ 'Helicobacter_pylori_hm4'       } = [qw(hm4)];
		$SiteDefs::__species_aliases{ 'Helicobacter_pylori_hm5'       } = [qw(hm5)];
		$SiteDefs::__species_aliases{ 'Helicobacter_pylori_hm6'       } = [qw(hm6)];
		$SiteDefs::__species_aliases{ 'Helicobacter_pylori_hm7'       } = [qw(hm7)];
		.....
		
##	使用Ensembl Bacteria Site
----
*	Plugins.pm
	
		$SiteDefs::ENSEMBL_PLUGINS = [
		  'EnsEMBL::Mirror'     => $SiteDefs::ENSEMBL_SERVERROOT.'/public-plugins/mirror',
		  'EnsEMBL::Genoverse'  => $SiteDefs::ENSEMBL_SERVERROOT.'/public-plugins/genoverse',
		  'EG::Bacteria' => $SiteDefs::ENSEMBL_SERVERROOT.'/eg-web-bacteria',
		  'EG::Common' => $SiteDefs::ENSEMBL_SERVERROOT.'/eg-web-common',
		];

*	vim eg-web-bacteria/modules/EnsEMBL/Web/Configuration/Location.pm 
	 
		sub modify_tree {
		  my $self = shift;
		  $self->delete_node('Marker');
		#  $self->delete_node('Synteny'); # wally
		#  $self->delete_node('Compara'); # wally
		  $self->delete_node('Variation');
		}

*	 vim public-plugins/mirror/conf/ini-files/DEFAULTS.ini
*	 
		; path to ttfonts remember /
		GRAPHIC_TTF_PATH     = /nfs/public/rw/ensembl/fonts/truetype/msttcorefonts/
		; Install from corefonts.sourceforge.net

*	vim eg-web-common/conf/ini-files/DEFAULTS.ini 
*	
		line 46
		ENSEMBL_CALC_GENOTYPES_FILE   =  /nfs/public/rw/ensembl/tools/calc_genotypes

###	Fix tree hyperlink error: "View fully expended tree"
*	vim ensembl-webcode/modules/EnsEMBL/Web/Component/Gene/ComparaTree.pm

		in line 324
		#push @view_links, sprintf $li_tmpl, $hub->url({ collapse => 'none', g1 => $highlight_gene }), 'View fully expanded tree';

		=>
	
		push @view_links, sprintf $li_tmpl, $hub->url( { action => 'Compara_Tree', collapse => 'none', g1 => $highlight_gene }), 'View fully expanded tree';


###	Fix compara alignment show no result
*	vim /ensembl/ensembl-genomes/release-22/ensembl-webcode/modules/EnsEMBL/Web/SpeciesDefs.pm 
	
		line 234
		sub get_config {
			my $self    = shift;
			my $species = shift;
			$species = lc($species);  #add by wally

*	原因:get_config時候Helicobacter -> 大寫，但在ini 及Site.Def中都為小寫，get\_config辨認不到

		
##	啟動Ensembl網站
----
*	在/ensembl/ensembl-genomes/release-22目錄下

	*	執行eg-restart
	
	
	


*	查看Log檔案
	*	/ensembl/ensembl-genomes/release-22logs/localhost.localdomain.error_log

*	重新設置後，重啟需刪除上次啟動時產生的設定檔案

		rm /ensembl/ensembl-genomes/current/ensembl-webcode/conf/config.packed 
		rm -rf /ensembl/ensembl-genomes/current/ensembl-webcode/conf/packed/*.packed
