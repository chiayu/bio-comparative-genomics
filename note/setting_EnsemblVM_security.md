# Setting Ensembl VM ##
----
##		Authorizaion to access web

----



###	vim /ensembl/ensembl-genomes/release-22/ensembl-webcode/conf/httpd.conf


*	Add some scripts
   
   
1.	Load modules (In line 15)
	
		

			LoadModule authz_user_module modules/mod_authz_user.so
			LoadModule auth_digest_module modules/mod_auth_digest.so
			LoadModule auth_basic_module modules/mod_auth_basic.so
	

2.	Access file 權限設置 (Add in end of http.con)

			AccessFileName .htaccess
			<Files ~ "^\.ht">
		  	Order allow,deny
	 	 	Deny from all
			</Files>

			<Directory "/ensembl/ensembl-genomes/release-22/ensembl-webcode/htdocs/">
	 	 	AllowOverride AuthConfig
	 		Order allow,deny
	 	 	Allow from all
			</Directory>
		
*	AccessFileName .htaccess => 告訴apache 要去.htaccess看Access要透過哪種方式
*	\<Files\>\</Files\> => 不允許任何連線至.ht檔(保護設定)
*	\<Directory\>\</Directory\> => 告訴apache 要去哪裡找.htaccess檔案


###	vim .htaccess

*	.htaccess 必須設定成/ensembl/ensembl-genomes/release-22/ensembl-webcode/htdocs/之下

*	Add scripts:
	
		AuthName     "Protected by Yang's lab"
		Authtype     Basic
		AuthUserFile /home/eguser/.htpasswd
		Require valid-user
		
	*	Authtype = 認證模式 可選用basic & digest
	
	*	AuthUserFile = 放使用者名稱和密碼的地方
	
	
### 產生.htpasswd

*	利用htpasswd產生
*	htpasswd /home/eguser/.htpasswd username passward