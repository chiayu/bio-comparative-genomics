
##  How to import VM on server ##

----

###  Import##

*   VBoxManage import EnsemblGenomes22_hp.ova --options keepallmacs --vsys 0 --vmname "test_blast"


###  Start VM ##


*   方法一: 使用VBoxManage
    
        VBoxManage startvm "EnsemblGenomes22_hp" -type headless
    
    -type headless（在沒有GUI下,必須使用）

    會在背景執行
    
    
    

*   方法二: 使用VBoxHeadless

    
        VBoxHeadless -startvm "EnsemblGenomes22_hp"


###  Check vms   ##

*   VBoxManage list runningvms

*   VBoxManage list vms

*   VBoxManage showvminfo "EnsemblGenomes22_hp"

###  Turn off VM##

*   VBoxManage controlvm "EnsemblGenomes22_hp" poweroff

### Delete VM ###
*   vboxmanage list vms 
    - "EnsemblGenomes22" {ade39620-5000-4691-b565-5b2f8316f3aa}
    - "EnsemblGenomes22_1" {d210e732-ae51-471a-bf96-ce6a80d2ab61}
*   想把多塞的vm 刪除

        vboxmanage unregistervm "EnsemblGenomes22_1" -delete


###  Set up VM port and other...##
*   VBoxManage modifyvm "EnsemblGenomes22_2" --natpf1 "guestssh,tcp,,8022,,22”
    
    設定VM網卡，port(ssh 會從8022連接到VM)
    
*   VBoxManage getextradata "EnsemblGenomes22_2" enumerate

    查看設定是否成功

    
###  User manual & forum
*   https://www.virtualbox.org/manual/
*   https://forums.virtualbox.org/



