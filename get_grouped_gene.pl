#!/usr/bin/perl
##Author: Chia-Yu Hsu
##Date: 2014-10-19
##Last update:  2015/7/28
##Environment:  Ensemble API version 75, BioPerl-1.6.923

=pod

=head1 NAME

       get_grouped_gene.pl 
       
=head1 SYNOPSIS
       
        perl get_grouped_gene.pl -conf [file ...] -group [group.txt]

       Options:
        -help                          show brief help message
        -test [gene_stable_id]         test mode, show the tree of the stable id 
        -group [group.txt]             necessary, which represents the group file
        -conf  [registry.conf]         necessary, which represents the registry.conf file

=head1 OPTIONS

=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-test> [gene_stable_id]

Print the tree and with corresponding <gene_stable_id>

=item B<-group>

    Example:conf/group_hm.txt
    helicobacter_pylori_hm1 
    helicobacter_pylori_hm2  
    helicobacter_pylori_hm4 
    helicobacter_pylori_hm5 
    helicobacter_pylori_hm6  

=item B<-conf> [gene_stable_id]

Registry.conf file

=back

=head1 DESCRIPTION
        
        To find gene which can be clustered based on protein tree, the output described as following:
        <gene desc>      <db>               <stable id>     <score>           <cluster id>
        ATPase    helicobacter_pylori_hm1  ENSHPG0000001      7  default
  
        <gene desc>         gene description
        <db>                from which database
        <stable id>         gene stable id
        <score>             cluster situation, the higher means that tree could be clusterd by group.
        <cluster id>        which method to generate tree, the tree shown on website is 'default'.
        
        The program will print all trees, you need to filter by yourself.

=head1 SCORE

        Score was calculated for every node for a specific tree, 
        so there would be a lot of scores for a single tree.        
        How to cauclated a score please see my master thesis <Chapter 3.10.2>.
        

        
=cut

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::GeneTree;
use Bio::EnsEMBL::Compara::MemberSet;
use Bio::EnsEMBL::Registry;
use Getopt::Long;
use GetCompara;
use Pod::Usage qw(pod2usage);

my ( $help, $test_stable_id, $conf_file, $group_file );

GetOptions(
	"-h|help" => \$help,
	"test=s"  => \$test_stable_id,
	"conf=s"  => \$conf_file,
	"group=s" => \$group_file,
);

pod2usage( -verbose => 2 ) if $help;

my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);

&test($test_stable_id) if $test_stable_id;
&main unless ($test_stable_id);

sub main {

	my $geneTree_adaptor =
	  $REGISTRY->get_adaptor( 'compara', 'compara', 'GeneTree' );

    #Specifiy the tree type
    #my @trees = @{$geneTree_adaptor->fetch_all( '-CLUSTERSET_ID' => "default" )};
	my @trees = @{ $geneTree_adaptor->fetch_all() };

	#Load the group.txt
	open( FILE, $group_file ) or die "$! cannot open $group_file";
	my %group = map { chomp; $_ => 1; } <FILE>;
	close FILE;

	foreach my $tree (@trees) {
		&check_tree_grouping( $tree, \%group );
	}

}

sub test {

	my $test_stable_id = shift;

	my $geneTree_adaptor =
	  $REGISTRY->get_adaptor( 'compara', 'compara', 'GeneTree' );
	my $member_adaptor =
	  Bio::EnsEMBL::Registry->get_adaptor( 'compara', 'compara', 'GeneMember' );
	my $member =
	  $member_adaptor->fetch_by_source_stable_id( 'ENSEMBLGENE',
		$test_stable_id );
	my $tree = $geneTree_adaptor->fetch_default_for_Member($member);


	$tree->print_tree;
	print "cluster id: " . $tree->clusterset_id() . "\n";
	print "align id: " . $tree->gene_align_id() . "\n";
	print "root id: " . $tree->root_id() . "\n";

}

sub check_tree_grouping {

	my $tree  = shift;
	my %group = %{ shift @_ };

	my $tree_nodes = $tree->get_all_nodes();
	foreach my $node ( @{$tree_nodes} ) {

		unless ( $node->is_leaf() ) {
			my $leaves = $node->get_all_leaves();
			&check_node_grouping( $leaves, \%group, $tree->clusterset_id );

		}
	}

}

sub check_node_grouping() {

	my $leaves_ref      = shift;
	my %group           = %{ shift @_ };
	my $tree_cluster_id = shift @_;

	my %tree_group = ();

	my $gene_adaptor;
	my $print_gene;
	my $print_db;
	my $print_stable_id;
	my $print_description;

	my @all_genes = ();

	foreach my $member (@$leaves_ref) {
		my $strain = $member->genome_db->name;
		$tree_group{$strain} = $tree_group{$strain} + 1;

		$gene_adaptor =
		  $REGISTRY->get_adaptor( $member->genome_db->name, "core", "gene" );
		$print_db        = $member->genome_db->name;
		$print_stable_id = $member->stable_id;
		push( @all_genes, $print_stable_id );
		$print_gene =
		  $gene_adaptor->fetch_by_translation_stable_id($print_stable_id);
		$print_description = $print_gene->description;

	}

	#check is grouping
	my $score = 0;
	foreach my $key ( keys %tree_group ) {

		if ( exists( $group{$key} ) ) {
			$score++;
		}
		else {
			$score--;
		}
	}

	if ( $score >= int( scalar( keys(%group) ) / 2 ) ) {
		&print_grouped_gene(
			$print_description, $print_db, $print_stable_id,
			$score,             $tree_cluster_id
		);

	}

}

sub print_grouped_gene {

	my ( $description, $db, $stable_id, $score, $tree_cluster_id ) = @_;

	print $description, "\t",
	  $db, "\t",
	  $stable_id, "\t", $score, "\t", $tree_cluster_id, "\n";

}
