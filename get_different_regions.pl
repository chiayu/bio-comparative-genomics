#!/usr/bin/env perl
##Author: Chia-Yu Hsu
##Date: 2015-04-13
##Last update:  2015/7/28
##Environment:  Ensemble API version 75, BioPerl-1.6.923

=pod

=head1 NAME

       get_different_regions.pl - Using GetCompara.pm,  
       -use Statistics::Test::WilcoxonRankSum;
       -use Statistics::Descriptive,

=head1 SYNOPSIS
       
        perl get_different_regions.pl -conf [file ...] -groups [group1.txt] [group2.txt] -ref [reference name] -window_size 2000

       Options:
        -help            brief help message
        -conf            file, registry.conf
        -groups          file, group1.txt,group2.txt
        -ref             string, helicobacter_pylori_du1
        -window_size     int, 2000
        -wilcoxon        test two samples 



=head1 OPTIONS

=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-conf>

Registry.conf file

=item B<-groups>

At least to enter two files, group1.txt,group2.txt

    conf/group_hm.txt:
    
    helicobacter_pylori_hm1 
    helicobacter_pylori_hm2  
    helicobacter_pylori_hm4 
    helicobacter_pylori_hm5 
    helicobacter_pylori_hm6  


=item B<-ref>

Enter the specific species you want to be the referensce, group will align to it.

=item B<-window_size>

Selected region size (bp) , and it will sliding (window size) / 2 

=item B<-wilcoxon>

Do the wilcoxon test.

=back



        
=head1 DESCRIPTION

        output as following:
        <seq region name>   <start> <end>   <group1 file name>   <sum>   <mean>  <std>  <group1 file name>   <sum>   <mean>  <std>...
        contig_u1 201   301   ulcer.txt 17.2 1.7 37.93 77  hm.txt    1347.0094   122.4554    1011.283353 74
        contig_u1 191   201   ulcer.txt 14.4 1.4 23.71 51  hm.txt    1329.786505 120.8896823 889.3734147 53
        output for Wilcoxon Test:
        Probability:   0.438580, normal approx w. mean: 121.000000, std deviation:  14.200939, z:  -0.774597
        
=cut

use strict;
use warnings;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Registry;
use Bio::AlignIO;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);
use Statistics::Descriptive;
use Statistics::Test::WilcoxonRankSum;
use GetCompara;

my ( $help, $conf_file, $ref_specie, @groups, $window_size, $wilcoxon );

GetOptions(
	'-help|h'        => \$help,
	'-conf=s'        => \$conf_file,
	'-ref=s'         => \$ref_specie,
	'-group=s{2,}'   => \@groups,
	'-window_size=i' => \$window_size,
	'-wilcoxon!'     => \$wilcoxon,
);

pod2usage( -verbose => 2 ) if $help;

my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);

&main;

sub main {

	my $alignment_type = "LASTZ_NET";

	my $slice_adaptor =
	  Bio::EnsEMBL::Registry->get_adaptor( $ref_specie, 'core', 'Slice' );

	my $slices = get_slices_from_sliding_window( $window_size, $slice_adaptor );

	#print and compare between groups slice by slice
	foreach my $slice ( @{$slices} ) {

		&print_slice_info($slice);

		my %data_for_Wincoxon = ();

		foreach my $group_file (@groups) {

			my $dif = &get_diff_from_ref_of_group( $slice, $group_file );
			&print_diff_for_group( $group_file, $dif );
			$data_for_Wincoxon{$group_file} = $dif->{'arr'};
		}

		if ($wilcoxon) {
			&do_Wincoxon_test( \%data_for_Wincoxon );
		}

		&print_slice_info();
	}

}

sub get_slices_from_sliding_window {

	my ( $window_size, $slice_adaptor ) = @_;

	my $all_slices = $slice_adaptor->fetch_all('seqlevel');

	my @total_slices_by_sliding = ();

	foreach my $one_slice (@$all_slices) {

		my $limit_length = ( $one_slice->length ) - 1 - $window_size;
		my $shift_size   = $window_size / 2;
		for ( my $i = 1 ; $i <= $limit_length ; $i = $i + $shift_size ) {

			my $slice =
			  $slice_adaptor->fetch_by_region( "contig",
				$one_slice->seq_region_name(),
				$i, $i + $window_size );
			$slice->seq;    # if I don't call it , it leads to MYSQL::st Error
			push( @total_slices_by_sliding, $slice );
		}

	}

	return \@total_slices_by_sliding;

}

sub get_diff_from_ref_of_group {

	my ( $slice, $group_file ) = @_;

	my $ref_specie = $slice->adaptor->db()->species();

	#Load the group.txt
	open( FILE, $group_file ) or die "$! cannot open $group_file";
	my @all_species = map { chomp; $_; } <FILE>;
	close FILE;

	my $compara = GetCompara->new(
		'-alignment_type' => 'LASTZ_NET',
		'-conf_file'      => $conf_file
	);
	my @ref_sa_ary;
	my $total_score   = 0;
	my $total_aln_num = 0;

	#count statistic difference
	my $stat         = Statistics::Descriptive::Full->new();
	my @arr_for_stat = ();

	foreach my $other_specie (@all_species) {
		if (   $other_specie ne $ref_specie
			&& $other_specie ne "helicobacter_pylori_sf7" )
		{

			my $ref_sa =
			  $compara->get_simple_align_to_another( $other_specie, $ref_specie,
				$slice );

			$total_score   = $total_score + $ref_sa->{'score'};
			$total_aln_num = $total_aln_num + $ref_sa->{"num_of_gabs"};

			$stat->add_data( $ref_sa->{'score'} );
			push( @arr_for_stat, $ref_sa->{'score'} );

			if ($ref_sa) {
				push( @ref_sa_ary, $ref_sa->{'ref_sa'} );
			}

		}
	}

	return {
		'score'       => $total_score,
		'num_of_gbas' => $total_aln_num,
		'ref_sa'      => \@ref_sa_ary,
		'mean'        => $stat->mean(),
		'variance'    => $stat->variance(),
		'sum'         => $stat->sum(),
		'arr'         => \@arr_for_stat,
	};

}

sub do_Wincoxon_test {

	my $hash_of_data = shift;

	my @ref_array   = values %{$hash_of_data};
	my $dataset_1   = shift @ref_array;
	my $dataset_2   = shift @ref_array;
	my $wilcox_test = Statistics::Test::WilcoxonRankSum->new();
	eval {

		$wilcox_test->load_data( $dataset_1, $dataset_2 );
		print $wilcox_test->probability() . "\t";
		print $wilcox_test->probability_status() . " \t";

	};

}

sub print_slice_info {
	my ($slice) = @_;
	if ($slice) {
		print $slice->seq_region_name . "\t"
		  . $slice->start . "\t"
		  . $slice->end . "\t";
	}
	else {
		print "\n";
	}
}

sub print_diff_for_group {

	my ( $group_file, $diff ) = @_;

	print $group_file, "\t",
	  $diff->{'score'} . "\t",
	  $diff->{'mean'} . "\t", $diff->{'variance'} . "\t",
	  $diff->{'num_of_gbas'} . "\t";

}

