#!/usr/bin/perl
##Author: Chia-Yu Hsu
##Date: 2014-10-19
##Last update:  2015/7/28
##Environment:  Ensemble API version 75, BioPerl-1.6.923

=pod

=head1 NAME

       get_gene_loss.pl - using GetCompara 

=head1 SYNOPSIS
       
        perl get_gene_loss.pl -conf [file ...]


=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-conf>

Registry.conf file

=item B<-criteria>

how many other species have this gene and your species didn't have, you defined "loss"
, default is (total species)- 1

=back        
        
        
=head1 DESCRIPTION
        
        This program will get gene gain from all species with corresponding conf file.
        print table to STDOUT as following:

        <loss_gene_id> <db> <description> <description> <num of align> <num of artifact>
        loss_gene_1   hm1 ATPase, ATPase subunit...  0       0
        
        <loss_gene_id>   - running number defined automatically
        <species>        - specie which has gene loss
        <description>    - how other species defined this gene
        <num of align>   - how many alignment other species can align to my specie
        <num of artfact> - the number represents how many other species consider 
                           that this gene loss is due to error.                 

=cut

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Registry;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);
use GetCompara;

my ( $conf_file, $criteria, $help );

GetOptions(
	"conf=s"     => \$conf_file,
	'criteria=i' => \$criteria,
	"help|h"     => \$help,
);

pod2usage( -verbose => 2 ) if $help;

my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);

&main;

sub main {

	my %STRAINS = map { $_ => 0 } @{ $REGISTRY->get_all_species() };

	#set criteria
	unless ($criteria) {
		$criteria = scalar( @{ $REGISTRY->get_all_species() } ) - 1;
	}

	foreach my $specie ( keys %STRAINS ) {

		&count_gene_loss( $specie, $criteria );
	}
}

sub count_gene_loss {
	
        my ( $my_specie, $criteria ) = @_;
	my $num_of_loss = 0;

	my $geneTree_adaptor =
	  $REGISTRY->get_adaptor( 'compara', 'compara', 'GeneTree' );
	my $trees = $geneTree_adaptor->fetch_all();

	foreach my $tree (@$trees) {
		my %counter = map { $_ => 0 } @{ $REGISTRY->get_all_species() };

		foreach my $member ( @{ $tree->get_all_leaves() } ) {
			my $this_strain = $member->genome_db->name();
			$counter{$this_strain} = $counter{$this_strain} + 1;
		}

		#count how many species in tree
		my $counts = 0;
		foreach my $strain ( keys %counter ) {

			if ( $counter{$strain} == 0 ) {
				$counts++;
			}

		}

		#when my_specie is not exists in this tree,we print
		if ( $counter{$my_specie} == 0 and $counts > $criteria ) {

			&print_gene_loss( $my_specie, $tree, $num_of_loss );
			$num_of_loss++;
		}
	}
}

sub print_gene_loss {

	my ( $my_specie, $tree, $num_of_loss ) = @_;

	my %description;
	my $tag_for_align          = "no_alignment_results";
	my $total_num_of_alignment = 0;
	my $num_of_artifact;

	foreach my $member ( @{ $tree->get_all_leaves() } ) {
		my $gene_adaptor =
		  $REGISTRY->get_adaptor( $member->genome_db->name(), 'core', 'gene' );
		my $gene =
		  $gene_adaptor->fetch_by_translation_stable_id( $member->name );
		$description{ $gene->description } =
		    $description{ $gene->description } . ", "
		  . $member->genome_db->name() . ":"
		  . $gene->display_id;

		#check this gene loss is really gene loss
		my $compara = GetCompara->new(
			'-alignment_type' => 'LASTZ_NET',
			'-conf_file'      => $conf_file
		);
		my $compara_sit =
		  $compara->get_simple_align_to_another( $my_specie,
			$member->genome_db->name(),
			$gene->display_id );
		$total_num_of_alignment =
		  $total_num_of_alignment + $compara_sit->{"is_artifact"};

		if ( $compara_sit->{"is_artifact"} eq "1" ) {
			$num_of_artifact++;
		}
	}

	print "gene_loss_"
	  . $num_of_loss . "\t"
	  . $my_specie . "\t"
	  . join( ", ", keys %description ) . "\t"
	  . $total_num_of_alignment . "\t"
	  . $num_of_artifact . "\n";

}

