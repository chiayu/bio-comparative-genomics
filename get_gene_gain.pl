#!/usr/bin/perl
##Author: Chia-Yu Hsu
##Date: 2014-10-17
##Last update:  2015/7/28
##Environment:  Ensemble API version 75, BioPerl-1.6.923

=pod

=head1 NAME

get_gene_gain.pl

=head1 SYNOPSIS
       
    perl get_gene_gain.pl -conf [file ...]
        
=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-conf>

Registry.conf file

=back


=head1 DESCRIPTION
        
This program will get gene gain from all species with corresponding conf file.
print table to STDOUT as following:

<db>  <stable_id> <description>


hm1   ENSHPG00002       hypothetical protein




=head1 EXAMPLE

perl get_gene_gain.pl -conf conf/registry.conf.23         

=cut

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Registry;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);

my ( $conf_file, $help );

GetOptions(
	'-conf=s' => \$conf_file,
	'-help' => \$help,
);

pod2usage( -verbose => 2 ) if $help;

my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);
my %STRAINS = map { $_ => 0 } @{ $REGISTRY->get_all_species() };

&main;

sub main {

	foreach my $strain ( keys %STRAINS ) {
          &print_gene_gain($strain);
	}

}

sub print_gene_gain {

	my $strain       = shift;
	my %all          = map { $_ => 0 } @{ $REGISTRY->get_all_species() };
	my $gene_adaptor = $REGISTRY->get_adaptor( $strain, "core", "gene" );
	my $genes        = $gene_adaptor->fetch_all_by_biotype("protein_coding");

	$all{$strain} = $all{$strain} + 1 ;

	  foreach my $gene (@$genes){

		my $member_adaptor =
		  Bio::EnsEMBL::Registry->get_adaptor( 'compara', 'compara',
			'GeneMember' );
		  my $member =
		  $member_adaptor->fetch_by_source_stable_id( 'ENSEMBLGENE',
			$gene->stable_id() );

		  my $gene_tree_adaptor =
		  Bio::EnsEMBL::Registry->get_adaptor( 'compara', 'compara',
			'GeneTree' );
		  my $tree = $gene_tree_adaptor->fetch_default_for_Member($member);

		  #Print the gene gain cause no trees with gene
		  unless ($tree) {

			print $strain. "\t"
			  . $gene->stable_id() . "\t"
			  . $gene->description . "\n";
			next;
		}

		#tag1. Check the tree's leaf(gene) is the same strain ?
		#tag2. check the tree's leaf comes from other strain(@ALL)?
		my $tag1   = 0;
		  my $tag2 = 0;
		  my $last_gene;

		  foreach my $member ( @{ $tree->get_all_leaves() } ) {
			my $this_strain = $member->genome_db->name;

			if ( $this_strain eq $strain ) {
				$last_gene = $member->stable_id;
				$tag1      = 1;
			}

			foreach my $compare ( keys %all ) {
				if ( $all{$compare} == 0 ) {
					if ( $compare eq $strain ) { next; }
					if ( $this_strain eq $compare ) {
						$tag2 = 1;
					}
				}
			}
		}

		#print the gene which tree'node all by itsekf
		if ( ( $tag1 eq 1 ) and ( $tag2 eq 0 ) ) {
			print $strain. "\t"
			  . $gene->stable_id() . "\t"
			  . $gene->description . "\n";
		}

	  }
}
