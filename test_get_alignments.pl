#!/usr/bin/env perl
##Author: Chia-Yu Hsu
##Date: 2015-04-13

=pod

=head1 NAME

       test_get_alignments.pl - Using GetCompara.pm,  
       -use Statistics::Test::WilcoxonRankSum;
       -use Statistics::Descriptive,

=head1 SYNOPSIS
       
        perl get_alignments.pl -conf [file ...] -groups [group1.txt] [group2.txt] -ref [reference name] -window_size 2000
        perl test_get_alignments.pl -group group_hm.txt group_ulcer.txt -window_size 2000 -start 27001 -end 28001 -seq_region scf7180000000002_u1 -conf conf/registry.conf.23 -ref helicobacter_pylori_du1 2>err2

       Options:
        -help            brief help message
        -conf            file, registry.conf
        -groups          file, group1.txt,group2.txt
        -ref             string, helicobacter_pylori_du1
        -window_size     int, 2000
        -wilcoxon        test two samples 
        
=head1 DESCRIPTION

        output as following:
        <seq region name>   <start> <end>   <group1 file name>   <sum>   <mean>  <std>  <group1 file name>   <sum>   <mean>  <std>...
        contig_u1 201   301   ulcer.txt 17.2 1.7 37.93 77  hm.txt    1347.0094   122.4554    1011.283353 74
        contig_u1 191   201   ulcer.txt 14.4 1.4 23.71 51  hm.txt    1329.786505 120.8896823 889.3734147 53
        output for Wilcoxon Test:
        Probability:   0.438580, normal approx w. mean: 121.000000, std deviation:  14.200939, z:  -0.774597
        
=cut

use strict;
use warnings;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Registry;
use Bio::AlignIO;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);
use Statistics::Descriptive;
use Statistics::Test::WilcoxonRankSum;
use GetCompara;

my ( $help, $conf_file, $ref_specie, @groups, $window_size, $wilcoxon );
my ( $start, $end, $seq_region_name );
my $stat_mode;
GetOptions(
	'-h'             => \$help,
	'-conf=s'        => \$conf_file,
	'-ref=s'         => \$ref_specie,
	'-group=s{2,}'   => \@groups,
	'-window_size=i' => \$window_size,
	'-wilcoxon!'     => \$wilcoxon,
	'start=i'        => \$start,
	'end=i'          => \$end,
	'seq_region=s'   => \$seq_region_name,
	'-mode'          => \$stat_mode,
);

pod2usage( -verbose => 1 ) if $help;
my $REGISTRY = 'Bio::EnsEMBL::Registry';
$REGISTRY->load_all($conf_file);

&main;

sub main {

	my $alignment_type = "LASTZ_NET";
	my $slice_adaptor =
	  Bio::EnsEMBL::Registry->get_adaptor( $ref_specie, 'core', 'Slice' );


	my $sl =
	  $slice_adaptor->fetch_by_region( "contig", $seq_region_name, $start,
		$end );
	$sl->seq;
	my @slices = ($sl);

    my @ref_sa_ary;
	#print and compare between groups slice by slice
	foreach my $slice (@slices) {

		&print_slice_info($slice);

		my %data_for_Wincoxon = ();

		foreach my $group_file (@groups) {

			my $dif = &get_diff_from_ref_of_group( $slice, $group_file );
			&print_diff_for_group( $group_file, $dif );
			$data_for_Wincoxon{$group_file} = $dif->{'arr'};

			
			push( @ref_sa_ary,$dif->{'ref_sa'} );
			
		}

		if ($wilcoxon) {
			&do_Wincoxon_test( \%data_for_Wincoxon );
		}

		&print_slice_info();
	}

	#my $a_gene_aln = &merge_align( \@ref_sa_ary, $ref_specie );
	#&_output_aln_result( $a_gene_aln, 'clustalW' );
}

sub get_slices_from_sliding_window {

	my ( $window_size, $slice_adaptor ) = @_;

	my $all_slices = $slice_adaptor->fetch_all('seqlevel');

	my @total_slices_by_sliding = ();

	foreach my $one_slice (@$all_slices) {

		my $limit_length = ( $one_slice->length ) - 1 - $window_size;

		for ( my $i = 1 ; $i <= $limit_length ; $i = $i + 1000 ) {

			my $slice =
			  $slice_adaptor->fetch_by_region( "contig",
				$one_slice->seq_region_name(),
				$i, $i + $window_size );
			$slice->seq;    # if I don't call it , it leads to MYSQL::st Error
			push( @total_slices_by_sliding, $slice );
		}

	}

	return \@total_slices_by_sliding;

}

sub get_diff_from_ref_of_group {

	my ( $slice, $group_file ) = @_;

	my $ref_specie = $slice->adaptor->db()->species();

	#Load the group.txt
	open( FILE, $group_file ) or die "$! cannot open $group_file";
	my @all_species = map { chomp; $_; } <FILE>;
	close FILE;

	my $compara = GetCompara->new(
		'-alignment_type' => 'LASTZ_NET',
		'-conf_file'      => $conf_file
	);
	my @ref_sa_ary;
	my $total_score   = 0;
	my $total_aln_num = 0;

	#count statistic difference
	my $stat         = Statistics::Descriptive::Full->new();
	my @arr_for_stat = ();

	foreach my $other_specie (@all_species) {
		if (   $other_specie ne $ref_specie
			&& $other_specie ne "helicobacter_pylori_sf7" )
		{

			my $ref_sa =
			  $compara->get_simple_align_to_another( $other_specie, $ref_specie,
				$slice, $stat_mode );

			$total_score   = $total_score + $ref_sa->{'score'};
			$total_aln_num = $total_aln_num + $ref_sa->{"num_of_gabs"};

			$stat->add_data( $ref_sa->{'score'} );
			print "$ref_specie vs $other_specie :" . $ref_sa->{'score'} . "\n";
			push( @arr_for_stat, $ref_sa->{'score'} );

			if ($ref_sa) {
				push( @ref_sa_ary, $ref_sa->{'ref_sa'} );
			}

		}
	}

	return {
		'score'       => $total_score,
		'num_of_gbas' => $total_aln_num,
		'ref_sa'      => \@ref_sa_ary,
		'mean'        => $stat->mean(),
		'variance'    => $stat->variance(),
		'sum'         => $stat->sum(),
		'arr'         => \@arr_for_stat,
	};

}

sub do_Wincoxon_test {

	my $hash_of_data = shift;

	my @ref_array   = values %{$hash_of_data};
	my $dataset_1   = shift @ref_array;
	my $dataset_2   = shift @ref_array;
	my $wilcox_test = Statistics::Test::WilcoxonRankSum->new();
	$wilcox_test->load_data( $dataset_1, $dataset_2 );

	$wilcox_test->probability();
	print $wilcox_test->probability_status() . " \t";

}

sub print_slice_info {
	my ($slice) = @_;
	if ($slice) {
		print $slice->seq_region_name . "\t"
		  . $slice->start . "\t"
		  . $slice->end . "\t";
	}
	else {
		print "\n";
	}
}

sub print_diff_for_group {

	my ( $group_file, $diff ) = @_;

	print $group_file, "\t",
	  $diff->{'score'} . "\t",
	  $diff->{'mean'} . "\t", $diff->{'variance'} . "\t",
	  $diff->{'num_of_gbas'} . "\t";

}

sub merge_align {
	my $ref_sa_ary = shift;
	my $species    = shift;

	my $a_gene_aln;
	foreach my $this_aln (@$ref_sa_ary) {

		my $a_simpleAln = $this_aln;
		if ( !defined $a_gene_aln ) {
			$a_gene_aln = $a_simpleAln;
		}
		else {

			( $a_gene_aln, $a_simpleAln ) =
			  __perfect_simple_alns( $a_gene_aln, $a_simpleAln, $species );
			my $i = 2;
			while ( $i <= $a_simpleAln->num_sequences ) {

				$a_gene_aln->add_seq( $a_simpleAln->get_seq_by_pos($i) );
				$i++;
			}

		}
	}

	return $a_gene_aln;
}

sub __perfect_simple_alns {
	my $aln1    = shift;
	my $aln2    = shift;
	my $species = shift;

	my $ref_locseq1 = $aln1->get_seq_by_pos(1);
	my $ref_locseq2 = $aln2->get_seq_by_pos(1);

	my $ref_seq1 = $ref_locseq1->seq;
	my $ref_seq2 = $ref_locseq2->seq;

	my @ref_seq1 = split( //, $ref_seq1 );
	my @ref_seq2 = split( //, $ref_seq2 );

	my $aln_difference_pos = check_aln_start_position( $aln1, $aln2, $species );

	if ( $aln_difference_pos && $aln_difference_pos ne 0 ) {
		my $count = abs($aln_difference_pos);
		while ( $count > 0 ) {
			if ( $aln_difference_pos > 0 ) {
				unshift( @ref_seq2, '-' );
				$count--;
			}
			elsif ( $aln_difference_pos < 0 ) {
				unshift( @ref_seq1, '-' );
				$count--;
			}

		}
	}

	my $ref_seq1len     = scalar(@ref_seq1);
	my $ref_seq2len     = scalar(@ref_seq2);
	my $idx1            = 0;
	my $idx2            = 0;
	my %seq1_newgap_pos = ();
	my %seq2_newgap_pos = ();

	while ( $idx1 < $ref_seq1len || $idx2 < $ref_seq2len ) {

		#because original code will produce uninitalized value
		#so assinged the  value
		if ( !$ref_seq1[$idx1] ) {
			$ref_seq1[$idx1] = '';
			print $ref_seq1[$idx1];
		}
		elsif ( !$ref_seq2[$idx2] ) {
			$ref_seq2[$idx2] = '';
		}

		if ( ( $ref_seq1[$idx1] eq '-' ) xor( $ref_seq2[$idx2] eq '-' ) ) {
			if ( $ref_seq1[$idx1] eq '-' ) {
				$seq2_newgap_pos{$idx2}++;
				$idx1++;
			}
			else {
				$seq1_newgap_pos{$idx1}++;
				$idx2++;
			}
		}

		else {
			$idx1++;
			$idx2++;
		}
	}

	$aln1 = ___insert_gaps_into_simple_aln( $aln1, \%seq1_newgap_pos );
	$aln2 = ___insert_gaps_into_simple_aln( $aln2, \%seq2_newgap_pos );

	# exit;

	return $aln1, $aln2;
}

sub _output_aln_result {
	my $a_gene_aln    = shift;
	my $output_format = shift;
	my $alignIO       = Bio::AlignIO->newFh(

		#-interleaved => 0,
		-fh       => \*STDOUT,
		-format   => 'clustalW',
		-idlength => 40
	);
	print $alignIO $a_gene_aln;

	#foreach my $ref_sa (@$ref_sa_ary) {
	#print $alignIO $a_gene_aln;
	#}
}

sub check_aln_start_position {
	my ( $aln1, $aln2, $species ) = @_;
	my @compare_aln;

	foreach my $a ( keys %{ $aln1->{'_seq'} } ) {
		my @aln1_info = split( /\//, $a );
		if ( $aln1_info[0] eq $species ) {
			my @aln1_pos = split( '-', $aln1_info[2] );
			push( @compare_aln, $aln1_pos[0] );
		}
	}

	foreach my $a ( keys %{ $aln2->{'_seq'} } ) {
		my @aln2_info = split( /\//, $a );
		if ( $aln2_info[0] eq $species ) {
			my @aln2_pos = split( '-', $aln2_info[2] );
			push( @compare_aln, $aln2_pos[0] );
		}
	}

	if ( $compare_aln[0] ne $compare_aln[1] ) {
		my $aln_different = $compare_aln[0] - $compare_aln[1];
		return $aln_different;
	}
	else {
		return;
	}

}

sub ___insert_gaps_into_simple_aln {
	my $aln      = shift;
	my $gap_list = shift;

	if ( scalar( keys %$gap_list ) == 0 ) {
		return $aln;
	}

	my @seqs        = $aln->each_seq;
	my $ref_seq_txt = $seqs[0]->seq;
	my $ref_seq_len = length($ref_seq_txt);

	my @new_seqs_txt = ();
	for ( my $i = 0 ; $i < scalar(@seqs) ; $i++ ) {
		$new_seqs_txt[$i] = '';
		my @current_seq_txt = split( //, $seqs[$i]->seq );

		for ( my $j = 0 ; $j < $ref_seq_len ; $j++ ) {

			#because original code will produce uninitalized value
			#so assinged the  value
			if ( !$new_seqs_txt[$i] ) {
				$new_seqs_txt[$i] = '';
			}
			elsif ( !$current_seq_txt[$j] ) {
				$current_seq_txt[$j] = '';
			}

			if ( exists $gap_list->{$j} ) {
				$new_seqs_txt[$i] .= '-' x $gap_list->{$j};
			}
			$new_seqs_txt[$i] .= $current_seq_txt[$j];
		}
		$seqs[$i]->seq( $new_seqs_txt[$i] );

		#   $seqs[$i]->alphabet('dna');
	}

	# print 'seq0:'.$seqs[0]->seq, "\n";
	return $aln;
}
