##  How to use 
###	generate\_gene\_table.pl

	perl generate_gene_table.pl -conf registry.conf.23  -mode -t

###	get\_gene\_gain.pl

	perl get_gene_gain.pl -conf registry.conf.23 


###	perl get\_gene\_loss.pl
 
	perl get_gene_loss.pl -conf registry.conf.23 

###	get\_grouped\_gene.pl

	 perl get_grouped_gene.pl -conf conf/registry.conf.23 -group group_hm.txt

###	get\_different\_regions.pl 

	perl get_different_regions.pl --window_size 2000 -group /home/u00cyh00/project/ensembl_services/comparative_analysis/group_ulcer.txt /home/u00cyh00/project/ensembl_services/comparative_analysis/group_hm.txt --conf /home/u00cyh00/project/ensembl_services/comparative_analysis/conf/registry.conf.23 -wilcoxon -ref helicobacter_pylori_du1


###	get\_alignments\_seqs.pl 
	
	perl get_alignments_seqs.pl  -seq_region scf7180000000002_u1 -start 1208555 -end 1213962  -conf conf/registry.conf.23 -ref helicobacter_pylori_du1 -format clustalW 2>err

	Error output should be seperate.



### GetCompara.pm

				my $compara = GetCompara->new(
					'-alignment_type' => 'LASTZ_NET',
					'-conf_file'      => $conf_file
				);

#####	*get\_num\_of\_align\_from\_species()

	# Description: Get number of alignments of  gene A (in specieA) to other species
	# Return Type: number
	# Arg 1 : String, specieA
	# Arg 2 : String, stable id of gene A in specieA
	# Arg 3 : Hasf ref, which represent all the other strains

#####	*get\_simple\_align\_to\_another()

	# Description: Get alignment of slice (in spciecA) align to specieB.
	# Return Type: hash reference whiche contain {"ref_sa', 'score', 'num_of_gabs", "is_artifact"}
	# Arg 1 : String, spciecB
	# Arg 2 : String, spciecA
	# Arg 3 : Bio::EnsEMBL::Slice , slice of spciecsA or ENSEMBL GENE STABLE ID
	# Arg [4] : String, testing mode, default is undef



#####	*get\_diff\_from\_slice\_in_alignment\_level()


	# Description: Get difference of a slice (in spciecA) align to other species which written in group.txt.
	# Return Type: hash reference whiche contain {"ref_sa', 'score', 'num_of_gabs"}
	# Arg 1 : Bio::EnsEMBL::Slice
	# Arg [2] : String , reprenseting group.txt, if undef, it will count all groups.
	# Arg [3] : String , reprenseting exception_group.txt, default is null.
	# Arg [4] : String, testing mode, default is undef
